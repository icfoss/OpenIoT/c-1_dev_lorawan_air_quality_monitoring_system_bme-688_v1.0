
/**
  ******************************************************************************
  * @file	bsec_bme_688.c
  * @author CDOH Team
  * @brief  Driver for Air Quality Monitoring system
  *         This file provides firmware functions to manage the following
  *         functionalities
  *           + Initializes necessary pins for the sensors
  *           + Read data from the sensors
  * 		  + Outputs the data to transmit the collected data
  *@verbatim
  ==============================================================================
                        ##### How to use this driver #####
  ==============================================================================
 	 	 	(+) airQualityInit() to Initializes the Air Quality Monitoring system
 	 	 	(+) sensorRead() to Read the data from the bme688 sensor
 	 	 	(+) readBatteryLevel() to Read Battery status
 **/


/**
 * private Includes
 */

#include "bsec_bme_688.h"
#include "adc_if.h"


/* user defned variables */

struct bme68x_dev device;
uint32_t overflowCounter;
uint32_t lastTimeMS;
int64_t Time_stamp = 0;

uint8_t lastOpMode ;
uint8_t opMode;
float Sample_Rate = BSEC_SAMPLE_RATE_CONT;
float extTempOffset = 0.0f;

bsecOutputs outputs;
bsec_bme_settings_t  sensor_settings;


/**
 *
 * @brief Initializes Air quality mointoring system
 * @param none
 * @retval none
 *
 **/
void airQualityInit()
{
	int8_t rslt = BME68X_OK;

	MX_I2C1_Initi();

	GPIO_InitTypeDef initStruct = { 0 };
	initStruct.Pin = BATT_ENABLE_PIN;
	initStruct.Pull = GPIO_NOPULL;
	initStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	initStruct.Mode = GPIO_MODE_OUTPUT_PP;
	HAL_GPIO_Init(BATT_ENABLE_PORT, &initStruct);

	device.intf = BME68X_I2C_INTF;
	device.read = i2c_read;
	device.write = i2c_write;
	device.delay_us = delay;
	device.amb_temp = 25;
	rslt = bme68x_init(&device);
	bme68x_check_rslt("bme68x_init", rslt);
	if (rslt != BME68X_OK)
		return;
	rslt = BSEC_OK;
	rslt = bsec_init();
	bsec_check_rslt("bsec_init", rslt);
	if (rslt != BSEC_OK)
		return;

	sensor_settings.next_call = 0;

	lastOpMode = BME68X_SLEEP_MODE;
	HAL_Delay(2000);
}

/**
  * @brief  read external battery voltage connected to analog channel 4 through a voltage divider
  * @param  none
  * @retval battery voltage level
  */
uint16_t readBatteryLevel(void) {
	int analogValue = 0; /*   adc reading for battery is stored in the variable  */
	float batteryVoltage = 0;
	uint16_t batteryLevel = 0; /*    battery voltage   */

	/* enable battery voltage reading */
	enable(BATT_POWER);
	HAL_Delay(10);

	/* Read battery voltage reading */
	analogValue = ADC_ReadChannels(BATTERY_CHANNEL);

	/* disable battery voltage reading */
	disable(BATT_POWER);

	/*battery voltage = ADC value*Vref*2/4096   --12 bit ADC with voltage divider factor of 2 */
	batteryVoltage = (analogValue * 3.3 * ((R1 + R2) / R2)) / 4096;

	/*multiplication factor of 100 to convert to int from float*/
	batteryLevel = (uint16_t) (batteryVoltage * 100);

	return batteryLevel;
}

/**
 *
 * @brief manual control of gpio inorder control the MOSFET which allows the  battery power reading possible
 * @param the gpio to be enabled
 * @retval none
 *
 **/
void enable(uint8_t pin) {

	switch (pin) {
		case 1:
			HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_RESET); //for battery
			break;
}
}

/**
 * @brief manual control of gpio inorder to disable the power 
 * @param the gpio to be disabled
 * @retval none
 *
 **/
void disable(uint8_t pin) {

	switch (pin) {
		case 1:
			HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_SET); //for battery
			break;

}
}

/**
 *
 * @brief set the necessary configuration settings required for the bsec library
 * @param structure to store the data
 * @retval none
 *
 **/
void bsec(airQuality_t* sensor_data)
{


	bsec_library_return_t rslt = BSEC_OK;

	bsec_sensor_configuration_t requested_virtual_sensors[11];

	bsec_sensor_configuration_t required_sensor_settings[BSEC_MAX_PHYSICAL_SENSOR];

	uint8_t n_required_sensor_settings = BSEC_MAX_PHYSICAL_SENSOR;

	uint8_t n_requested_virtual_sensors = 11;


	requested_virtual_sensors[0].sensor_id = BSEC_OUTPUT_IAQ;
	requested_virtual_sensors[0].sample_rate = Sample_Rate;
	requested_virtual_sensors[1].sensor_id = BSEC_OUTPUT_CO2_EQUIVALENT;
	requested_virtual_sensors[1].sample_rate = Sample_Rate;
	requested_virtual_sensors[2].sensor_id = BSEC_OUTPUT_BREATH_VOC_EQUIVALENT;
	requested_virtual_sensors[2].sample_rate = Sample_Rate;
	requested_virtual_sensors[3].sensor_id = BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE;
	requested_virtual_sensors[3].sample_rate = Sample_Rate;
	requested_virtual_sensors[4].sensor_id = BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY;
	requested_virtual_sensors[4].sample_rate = Sample_Rate;
	requested_virtual_sensors[5].sensor_id = BSEC_OUTPUT_RAW_PRESSURE;
	requested_virtual_sensors[5].sample_rate = Sample_Rate;
	requested_virtual_sensors[6].sensor_id = BSEC_OUTPUT_RUN_IN_STATUS;
	requested_virtual_sensors[6].sample_rate = Sample_Rate;
	requested_virtual_sensors[7].sensor_id = BSEC_OUTPUT_RAW_GAS;
	requested_virtual_sensors[7].sample_rate = Sample_Rate;
	requested_virtual_sensors[8].sensor_id = BSEC_OUTPUT_STABILIZATION_STATUS;
	requested_virtual_sensors[8].sample_rate = Sample_Rate;
	requested_virtual_sensors[9].sample_rate = Sample_Rate;
	requested_virtual_sensors[9].sensor_id = BSEC_OUTPUT_RAW_TEMPERATURE;
	requested_virtual_sensors[10].sample_rate = Sample_Rate;
	requested_virtual_sensors[10].sensor_id = BSEC_OUTPUT_RAW_HUMIDITY;
	//bsec_sensor_configuration_t required_sensor_settings[BSEC_MAX_PHYSICAL_SENSOR];

	//uint8_t n_required_sensor_settings = BSEC_MAX_PHYSICAL_SENSOR;

	rslt = bsec_update_subscription(requested_virtual_sensors, n_requested_virtual_sensors,required_sensor_settings, &n_required_sensor_settings);
	bsec_check_rslt("bsec_update_subscription", rslt);
	if (rslt != BSEC_OK)
		return;

	BSEC_PROCESS(sensor_data);
}

/**
 *
 * @brief Process the data which is collected from the sensor
 * @param structure to store the data
 * @retval none
 *
 **/
void BSEC_PROCESS(airQuality_t* sensor_data)
{
	struct bme68x_data data;
	struct bme68x_conf conf;
	struct bme68x_heatr_conf heatr_conf;

	bsec_library_return_t rslt = BSEC_OK;

		Time_stamp = get_timestamp_us()* 1000;
	if(	Time_stamp >= sensor_settings.next_call)
	{
	rslt = bsec_sensor_control (Time_stamp,&sensor_settings );
	bsec_check_rslt("bsec_sensor_control", rslt);
	if (rslt != BSEC_OK)
		return;
	switch (sensor_settings.op_mode)
	{
	case BME68X_FORCED_MODE :
		APP_PRINTF("BME68X_FORCED_MODE\n\r");
		rslt = bme68x_get_conf(&conf, &device);
		bme68x_check_rslt("bme68x_get_conf",rslt);
		if(rslt != BME68X_OK)
			return;
		conf.os_hum = sensor_settings.humidity_oversampling;
		conf.os_temp = sensor_settings.temperature_oversampling;
		conf.os_pres = sensor_settings.pressure_oversampling;
		rslt = bme68x_set_conf(&conf,&device);
		bme68x_check_rslt("bme68x_set_conf",rslt);
		if(rslt != BME68X_OK)
			return;
		heatr_conf.enable = BME68X_ENABLE;
		heatr_conf.heatr_temp = sensor_settings.heater_temperature;
		heatr_conf.heatr_dur = sensor_settings.heater_duration;
		rslt = bme68x_set_heatr_conf(BME68X_FORCED_MODE,&heatr_conf,&device);
		bme68x_check_rslt("bme68x_set_heatr_conf",rslt);
		if(rslt != BME68X_OK)
			return;
		rslt = bme68x_set_op_mode(BME68X_FORCED_MODE,&device);
		bme68x_check_rslt("bme68x_set_op_mode",rslt);
		if(rslt != BME68X_OK)
			return;
		lastOpMode = BME68X_FORCED_MODE;
	    opMode = BME68X_FORCED_MODE;
	    break;

	case BME68X_PARALLEL_MODE :
		APP_PRINTF("BME68X_PARALLEL_MODE\n\r");
	    uint16_t sharedHeaterDur = 0;
		rslt = bme68x_get_conf(&conf, &device);
		bme68x_check_rslt("bme68x_get_conf",rslt);
		if(rslt != BME68X_OK)
			return;
		conf.os_hum = sensor_settings.humidity_oversampling;
		conf.os_temp = sensor_settings.temperature_oversampling;
		conf.os_pres = sensor_settings.pressure_oversampling;
		rslt = bme68x_set_conf(&conf,&device);
		bme68x_check_rslt("bme68x_set_conf",rslt);
		if(rslt != BME68X_OK)
			return;
		sharedHeaterDur = BSEC_TOTAL_HEAT_DUR - (bme68x_get_meas_dur(BME68X_FORCED_MODE, &conf, &device) / INT64_C(1000));
		heatr_conf.enable = BME68X_ENABLE;
		heatr_conf.heatr_temp_prof = sensor_settings.heater_temperature_profile;
		heatr_conf.heatr_dur_prof = sensor_settings.heater_duration_profile;
		heatr_conf.shared_heatr_dur = sharedHeaterDur;
		heatr_conf.profile_len = sensor_settings.heater_profile_len;
		rslt = bme68x_set_heatr_conf(BME68X_PARALLEL_MODE,&heatr_conf,&device);
		bme68x_check_rslt("bme68x_set_heatr_conf",rslt);
		if(rslt != BME68X_OK)
			return;
		rslt = bme68x_set_op_mode(BME68X_PARALLEL_MODE,&device);
		bme68x_check_rslt("bme68x_set_op_mode",rslt);
		if(rslt != BME68X_OK)
			return;
		lastOpMode = BME68X_PARALLEL_MODE;
	    opMode = BME68X_PARALLEL_MODE;
	    break;
	case BME68X_SLEEP_MODE :
		rslt = bme68x_set_op_mode(BME68X_PARALLEL_MODE,&device);
		bme68x_check_rslt("bme68x_set_op_mode",rslt);
		if(rslt != BME68X_OK)
			return;
		if ((rslt == BME68X_OK) && (opMode != BME68X_SLEEP_MODE))
				opMode = BME68X_SLEEP_MODE;
		break;
	}
	if (sensor_settings.op_mode != BME68X_SLEEP_MODE)
	{
	    bsec_input_t inputs[BSEC_MAX_PHYSICAL_SENSOR]; /* Temp, Pres, Hum & Gas */
	    uint8_t nInputs = 0;
		uint8_t n_fields = 0;

	     rslt = bme68x_get_data(lastOpMode, &data, &n_fields, &device);
	     bme68x_check_rslt("bme68x_get_data", rslt);

	     /* Checks all the required sensor inputs, required for the BSEC library for the requested outputs */
	     if (BSEC_CHECK_INPUT(sensor_settings.process_data, BSEC_INPUT_TEMPERATURE))
	     {
	         inputs[nInputs].sensor_id = BSEC_INPUT_HEATSOURCE;
	         inputs[nInputs].signal = extTempOffset;
	         inputs[nInputs].time_stamp = Time_stamp;
	         nInputs++;
	 #ifdef BME68X_USE_FPU
	         inputs[nInputs].signal = data.temperature;
	 #else
	         inputs[nInputs].signal = data.temperature / 100.0f;
	 #endif
	         inputs[nInputs].sensor_id = BSEC_INPUT_TEMPERATURE;
	         inputs[nInputs].time_stamp = Time_stamp;
	         nInputs++;
	     }
	     if (BSEC_CHECK_INPUT(sensor_settings.process_data, BSEC_INPUT_HUMIDITY))
	     {
	 #ifdef BME68X_USE_FPU
	         inputs[nInputs].signal = data.humidity;
	 #else
	         inputs[nInputs].signal = data.humidity / 1000.0f;
	 #endif
	         inputs[nInputs].sensor_id = BSEC_INPUT_HUMIDITY;
	         inputs[nInputs].time_stamp = Time_stamp;
	         nInputs++;
	     }
	     if (BSEC_CHECK_INPUT(sensor_settings.process_data, BSEC_INPUT_PRESSURE))
	     {
	         inputs[nInputs].sensor_id = BSEC_INPUT_PRESSURE;
	         inputs[nInputs].signal = data.pressure;
	         inputs[nInputs].time_stamp = Time_stamp;
	         nInputs++;
	     }
	     if (BSEC_CHECK_INPUT(sensor_settings.process_data, BSEC_INPUT_GASRESISTOR) &&
	             (data.status & BME68X_GASM_VALID_MSK))
	     {
	         inputs[nInputs].sensor_id = BSEC_INPUT_GASRESISTOR;
	         inputs[nInputs].signal = data.gas_resistance;
	         inputs[nInputs].time_stamp = Time_stamp;
	         nInputs++;
	     }
	     if (BSEC_CHECK_INPUT(sensor_settings.process_data, BSEC_INPUT_PROFILE_PART) &&
	             (data.status & BME68X_GASM_VALID_MSK))
	     {
	         inputs[nInputs].sensor_id = BSEC_INPUT_PROFILE_PART;
	         inputs[nInputs].signal = (opMode == BME68X_FORCED_MODE) ? 0 : data.gas_index;
	         inputs[nInputs].time_stamp = Time_stamp;
	         nInputs++;
	     }

	     if (nInputs > 0)
	     {

	         outputs.nOutputs = BSEC_NUMBER_OUTPUTS;
//	         memset(outputs.output, 0, sizeof(outputs.output));

	         /* Processing of the input signals and returning of output samples is performed by bsec_do_steps() */
	        rslt = bsec_do_steps(inputs, nInputs, outputs.output, &outputs.nOutputs);
	        bsec_check_rslt("bsec_do_steps",rslt);
	        if(rslt != BSEC_OK)
	        	return;
	        for (uint8_t index = 0; index < outputs.nOutputs; index++)
	        {
	        	//PRINTF("Inside FOR\n\r");
	            switch (outputs.output[index].sensor_id)
	            {
		            case BSEC_OUTPUT_IAQ:
		                if(outputs.output[index].accuracy == 0 || 1)
		                	Sample_Rate = BSEC_SAMPLE_RATE_CONT;
		                else if(outputs.output[index].accuracy == 2 )
		                	Sample_Rate = BSEC_SAMPLE_RATE_LP;
	                else if(outputs.output[index].accuracy == 3)
		                	Sample_Rate = BSEC_SAMPLE_RATE_ULP;
		                else
		                	continue;
		                break;
		            default :
		            	continue;
		        }
	            Time_stamp = outputs.output[index].time_stamp;
	        }
	        OUTPUT(sensor_data);
	     }
	}
	}
}

/**
 *
 * @brief Outputs the processed data
 * @param structure to store the data
 * @retval none
 *
 **/
void OUTPUT(airQuality_t* sensor_data)
{
	for (uint8_t index = 0; index < outputs.nOutputs; index++)
    {
		switch (outputs.output[index].sensor_id)
		        {
		            case BSEC_OUTPUT_IAQ:
		                sensor_data->IAQ = outputs.output[index].signal;
		                sensor_data->IAQ_Accuracy = outputs.output[index].accuracy;

		                APP_PRINTF("IAQ = %d\n\r",sensor_data->IAQ);
		                APP_PRINTF("IAQ_ACCURACY = %d\n\r",outputs.output[index].accuracy);
		                break;
		            case BSEC_OUTPUT_CO2_EQUIVALENT:
		                 sensor_data->Co2 = outputs.output[index].signal;
		                APP_PRINTF("Co2 = %d\n\r",sensor_data->Co2);
		                APP_PRINTF("Co2_ACCURACY = %d\n\r",outputs.output[index].accuracy);
		                break;
		            case BSEC_OUTPUT_BREATH_VOC_EQUIVALENT:
		                sensor_data->BVOC = (outputs.output[index].signal)*100;
		                APP_PRINTF("BVOC = %d\n\r",sensor_data->BVOC);
		                break;
		            case BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE:
		                sensor_data->Temperature = (outputs.output[index].signal)*100;
		                APP_PRINTF("temperature = %d\n\r",sensor_data->Temperature);
		                break;
		            case BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY:
		                sensor_data->Humidity = outputs.output[index].signal*100;
		                APP_PRINTF("humidity = %d\n\r",sensor_data->Humidity);
		                break;
		            case BSEC_OUTPUT_RAW_TEMPERATURE:
		                sensor_data->Raw_Temperature = (outputs.output[index].signal)*100;
		                APP_PRINTF("Raw temperature = %d\n\r",sensor_data->Raw_Temperature);
		                break;
		            case BSEC_OUTPUT_RAW_HUMIDITY:
		                sensor_data->Raw_Humidity = outputs.output[index].signal*100;
		                APP_PRINTF("Raw humidity = %d\n\r",sensor_data->Raw_Humidity);
		                break;
		            case BSEC_OUTPUT_RAW_PRESSURE:
		            	sensor_data->Pressure = outputs.output[index].signal;
		                APP_PRINTF("raw_pressure = %d\n\r",sensor_data->Pressure);
		                break;
		            case BSEC_OUTPUT_RUN_IN_STATUS:
		                sensor_data->R_Status = outputs.output[index].signal;
		                APP_PRINTF("run_in_status = %d\n\r",sensor_data->R_Status);
		                break;
		            case BSEC_OUTPUT_RAW_GAS:
		                sensor_data->Gas_Resistance = outputs.output[index].signal;
		                APP_PRINTF("raw_gas = %d\n\r",sensor_data->Gas_Resistance);
		                break;
		            case BSEC_OUTPUT_STABILIZATION_STATUS:
		            	sensor_data->S_Status = (uint8_t)outputs.output[index].signal;
		                APP_PRINTF("stabilization status = %d\n\r",sensor_data->S_Status);
		                break;
		            default:
		                continue;
		        }
		        Time_stamp = outputs.output[index].time_stamp;
		        //PRINTF("Time_STAMP :%d\t\n\r",Time_stamp/1000);

    }
}

/**
 *
 * @brief Outputs the errors that  occurs during the collection of sensor data from bme688
 * @param Function name , variable to store result
 * @retval none
 *
 **/
void bme68x_check_rslt(const char api_name[], int8_t rslt)
{
    switch (rslt)
    {
        case BME68X_OK:
	        APP_PRINTF("API name [%s] OK\n\r",api_name);
            break;
        case BME68X_E_NULL_PTR:
            APP_PRINTF("API name [%s]  Error [%d] : Null pointer\r\n", api_name, rslt);
            break;
        case BME68X_E_COM_FAIL:
            APP_PRINTF("API name [%s]  Error [%d] : Communication failure\r\n", api_name, rslt);
            break;
        case BME68X_E_INVALID_LENGTH:
            APP_PRINTF("API name [%s]  Error [%d] : Incorrect length parameter\r\n", api_name, rslt);
            break;
        case BME68X_E_DEV_NOT_FOUND:
            APP_PRINTF("API name [%s]  Error [%d] : Device not found\r\n", api_name, rslt);
            break;
        case BME68X_E_SELF_TEST:
            APP_PRINTF("API name [%s]  Error [%d] : Self test error\r\n", api_name, rslt);
            break;
        case BME68X_W_NO_NEW_DATA:
            APP_PRINTF("API name [%s]  Warning [%d] : No new data found\r\n", api_name, rslt);
            break;
        default:
            APP_PRINTF("API name [%s]  Error [%d] : Unknown error code\r\n", api_name, rslt);
            break;
   }
}

/**
 *
 * @brief Outputs the errors that  occurs during the processing of data inside the bsec library
 * @param Function name , variable to store result
 * @retval none
 *
 **/
void bsec_check_rslt(const char api_name[], int8_t rslt)
{
	switch(rslt)
	{
	case BSEC_OK :
		APP_PRINTF("API name [%s] OK\n\r",api_name);
		break;
	case BSEC_E_DOSTEPS_INVALIDINPUT :
		APP_PRINTF("API name [%s] Error [%d] :Input (physical) sensor id passed to bsec_do_steps() is not in the valid range or not valid for requested virtual sensor\n\r",api_name,rslt);
		break;
	case BSEC_E_DOSTEPS_VALUELIMITS :
		APP_PRINTF("API name [%s] Error [%d] :		Value of input (physical) sensor signal passed to bsec_do_steps() is not in the valid range \n\r",api_name);
		break;
	case BSEC_W_DOSTEPS_TSINTRADIFFOUTOFRANGE :
		APP_PRINTF("API name [%s] Error [%d] :		Past timestamps passed to bsec_do_steps()\n\r",api_name,rslt);
		break;
	case BSEC_E_DOSTEPS_DUPLICATEINPUT :
		APP_PRINTF("API name [%s] Error [%d] :		Duplicate input (physical) sensor ids passed as input to bsec_do_steps()\n\r",api_name,rslt);
		break;
	case BSEC_I_DOSTEPS_NOOUTPUTSRETURNABLE :
		APP_PRINTF("API name [%s] Error [%d] :		No memory allocated to hold return values from bsec_do_steps(), i.e., n_outputs == 0\n\r",api_name,rslt);
		break;
	case BSEC_W_DOSTEPS_EXCESSOUTPUTS :
		APP_PRINTF("API name [%s] Error [%d] :		Not enough memory allocated to hold return values from bsec_do_steps(), i.e., n_outputs < maximum number of requested output (virtual) sensors\n\r",api_name,rslt);
		break;
	case BSEC_W_DOSTEPS_GASINDEXMISS :
		APP_PRINTF("API name [%s] Error [%d] :		Gas index not provided to bsec_do_steps()\n\r",api_name,rslt);
		break;
	case BSEC_E_SU_WRONGDATARATE :
		APP_PRINTF("API name [%s] Error [%d] :		The sample_rate of the requested output (virtual) sensor passed to bsec_update_subscription() is zero\n\r",api_name,rslt);
		break;
	case BSEC_E_SU_SAMPLERATELIMITS :
		APP_PRINTF("API name [%s] Error [%d] :		The sample_rate of the requested output (virtual) sensor passed to bsec_update_subscription() does not match with the sampling rate allowed for that sensor\n\r",api_name,rslt);
		break;
	case BSEC_E_SU_DUPLICATEGATE :
		APP_PRINTF("API name [%s] Error [%d] :		Duplicate output (virtual) sensor ids requested through bsec_update_subscription()\n\r",api_name,rslt);
		break;
	case BSEC_E_SU_INVALIDSAMPLERATE :
		APP_PRINTF("API name [%s] Error [%d] :		The sample_rate of the requested output (virtual) sensor passed to bsec_update_subscription() does not fall within the global minimum and maximum sampling rates\n\r",api_name,rslt);
		break;
	case BSEC_E_SU_GATECOUNTEXCEEDSARRAY :
		APP_PRINTF("API name [%s] Error [%d] :		Not enough memory allocated to hold returned input (physical) sensor data from bsec_update_subscription(), i.e., n_required_sensor_settings ::BSEC_MAX_PHYSICAL_SENSOR\n\r",api_name,rslt);
		break;
	case BSEC_E_SU_SAMPLINTVLINTEGERMULT :
		APP_PRINTF("API name [%s] Error [%d] :		The sample_rate of the requested output (virtual) sensor passed to bsec_update_subscription() is not correct\n\r",api_name,rslt);
		break;
	case BSEC_E_SU_MULTGASSAMPLINTVL :
		APP_PRINTF("API name [%s] Error [%d] :		The sample_rate of the requested output (virtual), which requires the gas sensor, is not equal to the sample_rate that the gas sensor is being operated\n\r",api_name,rslt);
		break;
	case BSEC_E_SU_HIGHHEATERONDURATION :
		APP_PRINTF("API name [%s] Error [%d] :		The duration of one measurement is longer than the requested sampling interval\n\r",api_name,rslt);
		break;
	case BSEC_W_SU_UNKNOWNOUTPUTGATE :
		APP_PRINTF("API name [%s] Error [%d] :		Output (virtual) sensor id passed to bsec_update_subscription() is not in the valid range; e.g., n_requested_virtual_sensors > actual number of output (virtual) sensors requested\n\r",api_name,rslt);
		break;
	case BSEC_W_SU_MODINNOULP :
		APP_PRINTF("API name [%s] Error [%d] :		ULP plus can not be requested in non-ulp mode\n\r",api_name,rslt);
		break;
	case BSEC_I_SU_SUBSCRIBEDOUTPUTGATES :
		APP_PRINTF("API name [%s] Error [%d] :		No output (virtual) sensor data were requested via bsec_update_subscription()\n\r",api_name,rslt);
		break;
	case BSEC_I_SU_GASESTIMATEPRECEDENCE :
		APP_PRINTF("API name [%s] Error [%d] :		GAS_ESTIMATE is suscribed and take precedence over other requested outputs\n\r",api_name,rslt);
		break;
	case BSEC_E_PARSE_SECTIONEXCEEDSWORKBUFFER :
		APP_PRINTF("API name [%s] Error [%d] :		n_work_buffer_size passed to bsec_set_[configuration/state]() not sufficient\n\r",api_name,rslt);
		break;
	case BSEC_E_CONFIG_FAIL :
		APP_PRINTF("API name [%s] Error [%d] :		Configuration failed\n\r",api_name,rslt);
		break;
	case BSEC_E_CONFIG_VERSIONMISMATCH :
		APP_PRINTF("API name [%s] Error [%d] :		Version encoded in serialized_[settings/state] passed to bsec_set_[configuration/state]() does not match with current version\n\r",api_name,rslt);
		break;
	case BSEC_E_CONFIG_FEATUREMISMATCH :
		APP_PRINTF("API name [%s] Error [%d] :		Enabled features encoded in serialized_[settings/state] passed to bsec_set_[configuration/state]() does not match with current library implementation\n\r",api_name,rslt);
		break;
	case BSEC_E_CONFIG_CRCMISMATCH :
		APP_PRINTF("API name [%s] Error [%d] :		serialized_[settings/state] passed to bsec_set_[configuration/state]() is corrupted\n\r",api_name,rslt);
		break;
	case BSEC_E_CONFIG_EMPTY :
		APP_PRINTF("API name [%s] Error [%d] :		n_serialized_[settings/state] passed to bsec_set_[configuration/state]() is to short to be valid\n\r",api_name,rslt);
		break;
	case BSEC_E_CONFIG_INSUFFICIENTWORKBUFFER :
		APP_PRINTF("API name [%s] Error [%d] :		Provided work_buffer is not large enough to hold the desired string\n\r",api_name,rslt);
		break;
	case BSEC_E_CONFIG_INVALIDSTRINGSIZE :
		APP_PRINTF("API name [%s] Error [%d] :		String size encoded in configuration/state strings passed to bsec_set_[configuration/state]() does not match with the actual string size n_serialized_[settings/state] passed to these functions\n\r",api_name,rslt);
		break;
	case BSEC_E_CONFIG_INSUFFICIENTBUFFER :
		APP_PRINTF("API name [%s] Error [%d] :		String buffer insufficient to hold serialized data from BSEC library\n\r",api_name,rslt);
		break;
	case BSEC_E_SET_INVALIDCHANNELIDENTIFIER :
		APP_PRINTF("API name [%s] Error [%d] :		Internal error code, size of work buffer in setConfig must be set to #BSEC_MAX_WORKBUFFER_SIZE\n\r",api_name,rslt);
		break;
	case BSEC_E_SET_INVALIDLENGTH :
		APP_PRINTF("API name [%s] Error [%d] :		Internal error code\n\r",api_name,rslt);
		break;
	case BSEC_W_SC_CALL_TIMING_VIOLATION :
		APP_PRINTF("API name [%s] Error [%d] :		Difference between actual and defined sampling intervals of bsec_sensor_control() greater than allowed\n\r",api_name,rslt);
		break;
	case BSEC_W_SC_MODEXCEEDULPTIMELIMIT :
		APP_PRINTF("API name [%s] Error [%d] : 		ULP plus is not allowed because an ULP measurement just took or will take place\n\r",api_name,rslt);
		break;
	case BSEC_W_SC_MODINSUFFICIENTWAITTIME :
		APP_PRINTF("API name [%s] Error [%d] :		ULP plus is not allowed because not sufficient time passed since last ULP plus\n\r",api_name,rslt);
		break;
	default:
	    APP_PRINTF("API name [%s]  Error [%d] : Unknown error code\r\n", api_name, rslt);
        break;
	}
}

/**
 *
 * @brief Read Sensor Data
 * @param structure to store the data
 * @retval none
 *
 **/
void sensorRead(airQuality_t* sensor_data)
{
	bsec(sensor_data);
	sensor_data->batteryLevel = readBatteryLevel();
	OUTPUT(sensor_data);
}

/**
 *
 * @brief collects the mcu time in micro second
 * @param 
 * @retval time in micro second
 *
 **/
int64_t get_timestamp_us()
{

    int64_t timeMs = (SysTimeToMs(SysTimeGetMcuTime()))*1000;

    if (lastTimeMS > timeMs) /* An overflow occurred */
    {
        overflowCounter++;
    }
    lastTimeMS = timeMs;

    return timeMs + (overflowCounter * INT64_C(0xFFFFFFFF));
}
