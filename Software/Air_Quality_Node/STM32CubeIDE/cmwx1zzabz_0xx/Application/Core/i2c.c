/**
  ******************************************************************************
  * @file	i2c.c
  * @author CDOH Team
  * @brief  Driver for i2c communication
  *         This file provides firmware functions to manage the following
  *         functionalities
  * 		  + configures the necessary steps to enable the i2c communication
  *@verbatim
  ==============================================================================
                        ##### How to use this driver #####
  ==============================================================================
 	 	 	(+) MX_I2C1_Initi() to Initializes the I2c peripheral
 	 	 	(+) i2c_read() to Read the data from the bme688 sensor via i2c
 	 	 	(+) i2c_write() to writes the data to the bme688 sensor via i2c
 	 	 	(+) delay() provides delay in micro seconds

 **/


/**
 * private includes
*/
#include "i2c.h"
#include "sys_app.h"
#include "main.h"


int8_t rslt = BME68X_OK ;


/**
 *
 * @brief Initializes i2c peripheral
 * @param none
 * @retval none
 *
 **/

void MX_I2C1_Initi(void)
{

  /* USER CODE END I2C2_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00707CBB;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }


APP_PRINTF("end of i2c init\n\r");
}

/**
 *
 * @brief enables i2c
 * @param I2C_HandleTypeDef
 * @retval none
 *
 **/
void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(hi2c->Instance==I2C1)
  {

    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**I2C1 GPIO Configuration
    PB9     ------> I2C1_SDA
    PB8     ------> I2C1_SCL
    */
    GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_8;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* Peripheral clock enable */
    __HAL_RCC_I2C1_CLK_ENABLE();

  }

}

/**
 *
 * @brief disables i2c
 * @param I2C_HandleTypeDef
 * @retval none
 *
 **/
void HAL_I2C_MspDeInit(I2C_HandleTypeDef* hi2c)
{
  if(hi2c->Instance==I2C1)
  {

    /* Peripheral clock disable */
    __HAL_RCC_I2C1_CLK_DISABLE();

    /**I2C1 GPIO Configuration
    PB9     ------> I2C1_SDA
    PB8     ------> I2C1_SCL
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_9);

    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_8);

  }

}
int i2c_chipIDTestRead()
{
	uint8_t value;
	uint8_t dev;
	//chip id register location
	uint8_t addr=0xd0;
	// bme280 device address
	dev = (0x77<<1);
	HAL_Delay(1);
	//to check whether the device is ready checking for ACK
	if(HAL_I2C_IsDeviceReady(&hi2c1,dev,4,100)== HAL_OK)
	{
   	HAL_I2C_Mem_Read(&hi2c1,dev,addr, 1,&value,1,100);
	}
	return value ;
}

/**
 *
 * @brief reads data through i2c
 * @param reg_addr, reg_data, data length, intf_ptr
 * @retval status
 *
 **/
int8_t i2c_read(uint8_t reg_addr, uint8_t *reg_data, uint32_t length, void *intf_ptr)
{

	/* Return 0 for Success, non-zero for failure */
		if (HAL_I2C_Mem_Read(&hi2c1, (0x77<<1), reg_addr, I2C_MEMADD_SIZE_8BIT,
				reg_data, length, 500) == HAL_OK) {
			rslt = 0;
		} else
			rslt = 1;

		return rslt;
}

/**
 *
 * @brief writes data through i2c
 * @param reg_addr, reg_data, data length, intf_ptr
 * @retval status
 *
 **/
int8_t i2c_write(uint8_t reg_addr,uint8_t *reg_data, uint32_t length, void *intf_ptr)
{
	rslt = 0; /* Return 0 for Success, non-zero for failure */

	if (HAL_I2C_Mem_Write(&hi2c1,(0x77<<1), reg_addr, 1, reg_data, length,
			5000) == HAL_OK)
	{
		rslt = 0;
	}
	else
	rslt = 1;


return rslt;
}

/**
 *
 * @brief generate delay in microseconds
 * @param delay period in microseconds, intf_ptr
 * @retval status
 *
 **/
void delay(uint32_t period_us, void *intf_ptr)
{
	(void) intf_ptr;
	HAL_Delay(period_us/1000);
}

/**
 * *********************************End of File*************************************
*/
