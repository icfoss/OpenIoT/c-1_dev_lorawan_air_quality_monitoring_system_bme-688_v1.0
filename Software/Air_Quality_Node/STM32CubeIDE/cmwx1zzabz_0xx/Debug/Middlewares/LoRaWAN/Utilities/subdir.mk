################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
/home/shafeekpm/Videos/GITLAB_PROJECTS/C-1_dev_LoRaWAN_air_quality_monitoring_system_v1.0/Software/Air_Quality_Node/Middlewares/Third_Party/LoRaWAN/Utilities/utilities.c 

OBJS += \
./Middlewares/LoRaWAN/Utilities/utilities.o 

C_DEPS += \
./Middlewares/LoRaWAN/Utilities/utilities.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/LoRaWAN/Utilities/utilities.o: /home/shafeekpm/Videos/GITLAB_PROJECTS/C-1_dev_LoRaWAN_air_quality_monitoring_system_v1.0/Software/Air_Quality_Node/Middlewares/Third_Party/LoRaWAN/Utilities/utilities.c Middlewares/LoRaWAN/Utilities/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DCMWX1ZZABZ0XX -c -I../../../LoRaWAN/App -I../../../LoRaWAN/Target -I"/home/shafeekpm/Videos/GITLAB_PROJECTS/C-1_dev_LoRaWAN_air_quality_monitoring_system_v1.0/Software/Air_Quality_Node/STM32CubeIDE/cmwx1zzabz_0xx/Drivers/BME688" -I../../../Core/Inc -I../../../Utilities/misc -I../../../Utilities/timer -I../../../Utilities/trace/adv_trace -I../../../Utilities/lpm/tiny_lpm -I../../../Utilities/sequencer -I../../../Drivers/BSP/B-L072Z-LRWAN1 -I../../../Drivers/BSP/CMWX1ZZABZ_0xx -I../../../Drivers/STM32L0xx_HAL_Driver/Inc -I../../../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../../../Drivers/CMSIS/Include -I../../../Middlewares/Third_Party/SubGHz_Phy -I../../../Middlewares/Third_Party/SubGHz_Phy/sx1276 -I../../../Middlewares/Third_Party/LoRaWAN/Crypto -I../../../Middlewares/Third_Party/LoRaWAN/Mac -I../../../Middlewares/Third_Party/LoRaWAN/Mac/Region -I../../../Middlewares/Third_Party/LoRaWAN/Utilities -I../../../Middlewares/Third_Party/LoRaWAN/LmHandler -I../../../Drivers/BSP/IKS01A2 -I../../../Drivers/BSP/Components/Common -I../../../Drivers/BSP/Components/hts221 -I../../../Drivers/BSP/Components/lps22hb -I../../../Drivers/BSP/Components/lsm6dsl -I../../../Drivers/BSP/Components/lsm303agr -I../../../Middlewares/Third_Party/LoRaWAN/LmHandler/Packages -Os -ffunction-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Middlewares-2f-LoRaWAN-2f-Utilities

clean-Middlewares-2f-LoRaWAN-2f-Utilities:
	-$(RM) ./Middlewares/LoRaWAN/Utilities/utilities.cyclo ./Middlewares/LoRaWAN/Utilities/utilities.d ./Middlewares/LoRaWAN/Utilities/utilities.o ./Middlewares/LoRaWAN/Utilities/utilities.su

.PHONY: clean-Middlewares-2f-LoRaWAN-2f-Utilities

