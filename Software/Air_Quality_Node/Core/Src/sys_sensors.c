/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    sys_sensors.c
  * @author  MCD Application Team
  * @brief   Manages the sensors on the application
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "stdint.h"
#include "sys_conf.h"
#include "sys_sensors.h"
#include "b_l072z_lrwan1_bus.h"
#include "sys_app.h"
#include "stm32_systime.h"

struct bme68x_dev device;
uint32_t overflowCounter;
uint32_t lastTimeMS;
int64_t Time_stamp = 0;

uint8_t lastOpMode ;
uint8_t opMode;
float Sample_Rate = BSEC_SAMPLE_RATE_CONT;
float extTempOffset = 0.0f;

bsecOutputs outputs;
bsec_bme_settings_t  sensor_settings;

/* Exported functions --------------------------------------------------------*/
void  EnvSensors_Read(sensor_data_t *sensor_data)
{
  /* USER CODE BEGIN EnvSensors_Read */

	/* USER CODE END EnvSensors_Read */
}

void  EnvSensors_Init(void)
{
  /* USER CODE BEGIN EnvSensors_Init */

	/* USER CODE END EnvSensors_Init */
}

/* USER CODE BEGIN EF */


/* USER CODE END EF */

/* Private Functions Definition -----------------------------------------------*/
/* USER CODE BEGIN PrFD */

/* USER CODE END PrFD */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
