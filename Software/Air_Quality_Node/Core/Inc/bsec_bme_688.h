/**
  ******************************************************************************
  * @file    bsec_bme_688.h
  * @author  CDOH Team
  * @brief   This file contains all the functions prototypes for the Rain Gauge station driver.
  ******************************************************************************
  */

#ifndef BSEC_BME_688_H_
#define BSEC_BME_688_H_

/**
 * Public includes
*/

#include "stdint.h"
#include "bme68x.h"
#include "bsec_interface.h"
#include "i2c.h"
#include "sys_app.h"
#include "bsec_interface_multi.h"
#include "stm32_systime.h"


/**
 * @defgroup Battery,BSEC
 */

#define BATT_POWER    				1

#define R1 	10		/*10 K resistor R1 in voltage divider*/
#define R2 	10		/*10 K resistor R2 in voltage divider*/

#define BATT_ENABLE_PORT		GPIOB
#define BATT_ENABLE_PIN			GPIO_PIN_2
#define BATTERY_CHANNEL			ADC_CHANNEL_4

#define BSEC_TOTAL_HEAT_DUR             UINT16_C(140)
#define BSEC_CHECK_INPUT(x, shift)		(x & (1 << (shift-1)))

/**
 * Structure to store sensor data
 */

typedef struct {
	int16_t 		Temperature;		//Compensated Temperature
	uint16_t		Humidity;			//Compensated Humidity
	int16_t 		Raw_Temperature;	//Raw Temperature
	uint16_t		Raw_Humidity;		//Raw Humidity
	uint32_t		Pressure;			//Pressure
	uint32_t		Gas_Resistance;		// Resistance value corresponding the sensed gas
	uint16_t		IAQ;				// Indoor air quality
	uint16_t		Co2;				// level of Carbon  Dioxide
	uint32_t		BVOC;				//	level of Breath VOC	
	uint8_t			S_Status;
	uint8_t			R_Status;
	uint8_t			IAQ_Accuracy;
	uint16_t 		batteryLevel; /* Battery Level */

} airQuality_t;


typedef struct
{
	bsec_output_t output[BSEC_NUMBER_OUTPUTS];
    uint8_t nOutputs;
} bsecOutputs;


/** @defgroup Air quality monitoring system Exported_Functions_Peripheral Control functions
  * @brief    Peripheral Control functions
  */
void airQualityInit();

void enable(uint8_t);

void disable(uint8_t);

void bsec(airQuality_t* sensor_data);

uint16_t readBatteryLevel(void);

void sensorRead(airQuality_t* sensor_data);

void BSEC_PROCESS(airQuality_t* sensor_data);

void OUTPUT(airQuality_t* sensor_data);

void bme68x_check_rslt(const char api_name[], int8_t rslt);

void bsec_check_rslt(const char api_name[], int8_t rslt);

int64_t get_timestamp_us();

#endif /* AIRQUALITY_BSEC_BME_688_H_ */
