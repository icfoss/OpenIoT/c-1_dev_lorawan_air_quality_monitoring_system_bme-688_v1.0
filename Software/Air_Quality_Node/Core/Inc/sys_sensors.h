/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    sys_sensors.h
  * @author  MCD Application Team
  * @brief   Header for sensors application
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SENSORS_H__
#define __SENSORS_H__

#ifdef __cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#include "bme68x.h"
#include "bsec_interface.h"
#include "bsec_interface_multi.h"
#include "adc_if.h"
/* USER CODE END Includes */
typedef struct
{
	int8_t 		Temperature;
	uint8_t		Humidity;
	uint32_t	Pressure;
	uint32_t	Gas_Resistance;
	uint16_t	IAQ;
	uint16_t	Co2;
	uint32_t	BVOC;
	uint8_t		S_Status;
	uint8_t		R_Status;
	uint8_t		IAQ_Accuracy;
	uint16_t 	batteryLevel; /* Battery Level */

} sensor_data_t;

/* Exported types ------------------------------------------------------------*/


typedef struct
{
	bsec_output_t output[BSEC_NUMBER_OUTPUTS];
    uint8_t nOutputs;
} bsecOutputs;

/**
  * Sensor data parameters
  */
#define BATT_POWER    				1

#define R1 	10		/*10 K resistor R1 in voltage divider*/
#define R2 	10		/*10 K resistor R2 in voltage divider*/

#define BATT_ENABLE_PORT		GPIOB
#define BATT_ENABLE_PIN			GPIO_PIN_2
#define BATTERY_CHANNEL			ADC_CHANNEL_4

#define BSEC_TOTAL_HEAT_DUR             UINT16_C(140)
#define BSEC_CHECK_INPUT(x, shift)		(x & (1 << (shift-1)))



/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
/**
  * @brief  initialize the environmental sensor
  */
void EnvSensors_Init(void);

/**
  * @brief  Environmental sensor  read.
  * @param  sensor_data sensor data
  */
void EnvSensors_Read(sensor_data_t *sensor_data);

/* USER CODE BEGIN EFP */
void enable(uint8_t);

void disable(uint8_t);

void bsec(sensor_data_t *sensor_data);



uint16_t readBatteryLevel(void);

void BSEC_PROCESS(sensor_data_t *sensor_data);

void OUTPUT(sensor_data_t *sensor_data);

void bme68x_check_rslt(const char api_name[], int8_t rslt);

void bsec_check_rslt(const char api_name[], int8_t rslt);

int64_t get_timestamp_us();

/* USER CODE END EFP */

#ifdef __cplusplus
}
#endif

#endif /* __SENSORS_H__ */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
