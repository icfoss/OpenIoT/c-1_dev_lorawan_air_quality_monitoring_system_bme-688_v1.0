/**
  ******************************************************************************
  * @file    i2c.h
  * @author  CDOH Team
  * @brief   This file contains all the functions prototypes for the Rain Gauge station driver.
  ******************************************************************************
  */

#ifndef I2C_H_
#define I2C_H_

#ifdef __cplusplus
 extern "C" {
#endif

/**
 * Public includes
*/

#include "bme68x_defs.h"
#include "stm32l0xx_hal.h"


extern I2C_HandleTypeDef hi2c1;

/** @defgroup i2c Exported_Functions_Peripheral Control functions
  * @brief    Peripheral Control functions
  */

void MX_I2C1_Initi(void);

void HAL_I2C_MspInit(I2C_HandleTypeDef*);

void HAL_I2C_MspDeInit(I2C_HandleTypeDef*);

int i2c_chipIDTestRead();

int8_t i2c_read(uint8_t reg_addr, uint8_t *reg_data, uint32_t length, void *intf_ptr);

int8_t i2c_write(uint8_t reg_addr,uint8_t *reg_data, uint32_t length, void *intf_ptr);

void delay(uint32_t period_us, void *intf_ptr);



#ifdef __cplusplus
}
#endif

#endif
