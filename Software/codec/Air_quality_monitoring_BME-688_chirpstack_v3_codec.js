
function Decode(fPort, bytes, variables) {
  if(fPort == 2)
  {
  var decoded = {};
  var i = 0;
  decoded.IAQ_ACCURACY = bytes[i++];
  if(decoded.IAQ_ACCURACY >0 )
  {
    decoded.IAQ = (bytes[i++]<<8) + bytes[i++];
    decoded.Co2 = (bytes[i++]<<8) + bytes[i++];
    decoded.BVOC = (bytes[i++]<<24) + (bytes[i++]<<16)+(bytes[i++]<<8) + bytes[i++];
    decoded.Temperature = ((bytes[i++]<<8) + bytes[i++])/100;
    decoded.Pressure = (bytes[i++]<<24) + (bytes[i++]<<16)+(bytes[i++]<<8) + bytes[i++];
    decoded.Humidity = ((bytes[i++]<<8) + bytes[i++])/100;
    decoded.Gas_Resistance = (bytes[i++]<<24) + (bytes[i++]<<16)+(bytes[i++]<<8) + bytes[i++];
    decoded.Bat_Level = (bytes[i++]<<8) + bytes[i++];
    decoded.Raw_Temperature = ((bytes[i++]<<8) + bytes[i++])/100;
    decoded.Raw_Humidity = ((bytes[i++]<<8) + bytes[i++])/100;
  }
  else
  {
    decoded.Bat_Level = (bytes[21]<<8) + bytes[22];
  }
  return decoded;
  }
  else
  {
  return {};
  }
}
