## C-1_dev_LoRaWAN_air_quality_monitoring_system_v1.0
---
![image-4.png](https://gitlab.com/abdularshadvs/c-1_dev_lorawan_air_quality_monitoring_system_bme-688_v1.0/-/raw/main/Images/image-4.png?ref_type=heads)
### _Overview_
---
The C-1 Dev LoRaWAN Air Quality Monitoring System v1.0 has been developed to provide an advanced solution for monitoring air quality. At its core is the BME688 sensor, renowned for its precision in measuring environmental parameters such as temperature, humidity, and pressure. Notably, it excels in providing accurate air quality data, including the detection of volatile organic compounds (VOCs) and other gases. This versatility makes the system ideal for diverse applications that demand a thorough understanding of ambient conditions, ranging from urban planning and industrial monitoring to environmental research.
The fully integrated Bosch BSEC library improves the precision of air quality measurements. This smart library maintains the accuracy of the sensor data by enabling real-time calibration and adjustment. As a result, users are able to make well-informed decisions based on the data gathered, due to a detailed and accurate assessment of environmental conditions.
The fully integrated Bosch BSEC library improves the precision of air quality measurements. This smart library ensures the accuracy of the sensor data by enabling real-time calibration and adjustment. As a result, users are able to make well-informed decisions based on the data gathered.
The system's core functionality revolves around the systematic collection of data at fixed intervals. This process is done by the Murata CMWX1ZZABZ microcontroller, embedded in the C-1 Dev board, which efficiently handles and stores sensor data. Subsequently, the data is transmitted seamlessly over LoRaWAN communication, offering a robust and long-range solution for data transfer. Leveraging the low-power consumption and reliable communication capabilities of LoRaWAN technology.

### _Features_
---
- LoRaWAN compatible
- Precision measurement of environmental parameters, including temperature, humidity, and pressure.
- Accurate detection of air quality indicators, such as volatile organic compounds (VOCs) and other gases.

### _Recorded Parameters_
---
- Indoor Air Quality
- Co2
- Breath VOC
- Temperature
- Humidity
- Pressure
- Gas Resistance
- Battery Voltage

### _Pre-Requisite_
---
- [STM32 CUBE IDE](https://www.st.com/en/development-tools/stm32cubeide.html)
- [C-1_Dev v1.0](https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0)
- [Gas Sensor BME688](https://www.adafruit.com/product/5046)
- [Battery](https://robu.in/product/panasonic-ncr-18650b-3400-mah-li-ion-battery/)

### _Getting Started_
---
- Connect the components as shown in the wiring diagram [here](https://gitlab.com/abdularshadvs/c-1_dev_lorawan_air_quality_monitoring_system_bme-688_v1.0/-/raw/main/Hardware/wiring%20diagram/wiring_diagram.png?ref_type=heads)
- Clone the repository 
 ``` sh 
 git clone https://gitlab.com/abdularshadvs/c-1_dev_lorawan_air_quality_monitoring_system_bme-688_v1.0.git
 ```
 - Open STM32 cube ide and create a workspace
 - Import the Air_Quality_Node folder from the cloned repository into the workspace
 ![image-5.png](https://gitlab.com/abdularshadvs/c-1_dev_lorawan_air_quality_monitoring_system_bme-688_v1.0/-/raw/main/Images/image-5.png?ref_type=heads)
- Configure the keys and address in se-identity.h
```c
// end-device IEEE EUI (big endian)
#define LORAWAN_DEVICE_EUI                  { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

// App/Join server IEEE EUI (big endian)
#define LORAWAN_JOIN_EUI                    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

// Set the device address on the network (big endian)
#define LORAWAN_DEVICE_ADDRESS              ( uint32_t )0x00000000
```
***NOTE***
- *For OTA devices, set the APP root key and leave others blank*
- *For ABP devices, set the Network root key,Network session key and Application session key*
- *For LoRaWAN version 1.0.x use same key for Network root  key and Network session key*

```c
// Application root key
#define LORAWAN_APP_KEY                     00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00

// Network root key
#define LORAWAN_NWK_KEY                     00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00

// Forwarding Network session key
#define LORAWAN_NWK_S_KEY                   00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00

// Application session key
#define LORAWAN_APP_S_KEY                   00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
```
- Set the Transmission Interval and Activation type (ABP/OTA) in lora_app.h
```c
// Defines the application data transmission duty cycle. 10s, value in [ms].
#define APP_TX_DUTYCYCLE                    10000
```
- Save changes and build the project
- Connect the c-1 dev to the ***ST-Link*** as shown in the programmer connection diagram [here](https://gitlab.com/abdularshadvs/c-1_dev_lorawan_air_quality_monitoring_system_bme-688_v1.0/-/tree/main/Hardware/Programmer%20connection%20diagram?ref_type=heads)
- click the debug option inside the stm32 cube ide to upload the program.

### _Acknowledgements_
---
- The project is built upon the base code of ST's I-CUBE-LRWAN package [here](https://www.st.com/en/embedded-software/i-cube-lrwan.html)
- Bosch BSEC 2.x sensor library and BME68x Sensor API is used to integrate the sensor [here](https://www.bosch-sensortec.com/software-tools/software/bme688-software/)

### _License_
---
This project is licensed under the MIT License - see the LICENSE.md file for details


